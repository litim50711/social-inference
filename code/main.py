from os.path import dirname, join
import numpy as np
import pandas as pd
from bokeh.io import output_file, show
from bokeh.models import Panel, Tabs
from bokeh.plotting import figure
from bokeh.io import curdoc
from bokeh.layouts import column, row
from bokeh.models import (Button, ColumnDataSource, CustomJS, DataTable, HTMLTemplateFormatter,RadioButtonGroup, Div,
                          NumberFormatter, Slider, TableColumn, TextInput)

df = pd.read_csv(join(dirname(__file__), 'hi-index/concat/HIindex_sort_{}_{}.csv'.format('0', '0.0')), names=["gender", "userID", "rank_value", "freq", "indegree"], nrows=100000)
source = ColumnDataSource(data=dict())
df_PR = pd.read_csv(join(dirname(__file__), 'pagerank/concat/Pagerank_sort_{}.csv'.format('0')), names=["gender", "userID", "rank_value", "freq", "indegree"], nrows=100000)
source_PR = ColumnDataSource(data=dict())
No_male = 0
No_female = 0
gender_dist = ColumnDataSource(dict(value=[No_female, No_male]))
gender_dist_PR = ColumnDataSource(dict(value=[No_female, No_male]))

def ratio_transfer(TR_TH, TR_PR):
    # like
    y_TH = 1.4709017299465927 * TR_TH + (-0.23905423597228675)
    y_PR = 2.260068224343442 * TR_PR + (-0.630582080270287)
    # comment
    y_TH_c = 1.6076362207515547 * TR_TH + (-0.3104812947414938)
    y_PR_c = 2.7391436344616076 * TR_PR + (-0.8381966907009905)
    return [y_TH, y_PR], [y_TH_c, y_PR_c]

def update():
    spinHTML="""<div class="loader" style="display: true"></div>"""
    formater =  HTMLTemplateFormatter(template="""<div class="loader" style="display: true"></div>""")
    ratios = ratio_transfer(TR_slider_TH.value, TR_slider_PR.value)[tabs.active]
    df = pd.read_csv(join(dirname(__file__), 'hi-index/concat/HIindex_sort_{}_{}.csv'.format(str(tabs.active), round((TR_slider_TH.value*10)/10, 2))), names=["gender", "userID", "rank_value", "freq", "indegree"], nrows=100000)
    sectors = df.groupby("gender")
    No_female = int(int(K_input.value)*ratios[0])
    female = sectors.get_group("F")[:No_female]
    No_male = int(K_input.value) - No_female
    male = sectors.get_group("M")[:No_male]
    gender_dist.data = {
        'value': [No_female, No_male]
    }
    
    current = pd.concat(
        [female, male]).sort_values(by=['rank_value'], ascending=False).dropna()
    source.data = {
        'gender': current.gender,
        'userID': current.userID,
        'rank_value': current.rank_value,
        'freq': current.freq,
        'indegree': current.indegree
    }
    
    df_PR = pd.read_csv(join(dirname(__file__), 'pagerank/concat/Pagerank_sort_{}.csv'.format(str(tabs.active))), names=["gender", "userID", "rank_value", "freq", "indegree"], nrows=100000)
    sectors = df_PR.groupby("gender")
    No_female = int(int(K_input.value)*ratios[1])
    female = sectors.get_group("F")[:No_female]
    No_male = int(K_input.value) - No_female
    male = sectors.get_group("M")[:No_male]
    gender_dist_PR.data = {
        'value': [No_female, No_male]
    }
    current_PR = pd.concat(
        [female, male]).sort_values(by=['rank_value'], ascending=False).dropna()
    source_PR.data = {
        'gender': current_PR.gender,
        'userID': current_PR.userID,
        'rank_value': current_PR.rank_value,
        'freq': current_PR.freq,
        'indegree': current_PR.indegree
    }

K_input = TextInput(value="1000", title="Seeds Size (K)")
K_input.on_change('value', lambda attr, old, new: update())

TR_slider_TH = Slider(start=0.2, end=0.8, value=0.2, step=.1, title="Target Ratio (Target HI-index)")
TR_slider_TH.on_change('value', lambda attr, old, new: update())

TR_slider_PR = Slider(start=0.4, end=0.6, value=0.4, step=.1, title="Target Ratio (PageRank)")
TR_slider_PR.on_change('value', lambda attr, old, new: update())

x = np.array([i/20 for i in range(1,21)])
y_TH = 1.4709017299465927 * x + (-0.23905423597228675)
y_PR = 2.260068224343442 * x + (-0.630582080270287)
p1 = figure(plot_width=400, plot_height=300,
    x_range=(0, 1), y_range=(0, 1),
    y_axis_label='Seeding Ratio (Female)',
    x_axis_label='Target Ratio (Female)')

p1.line( x, y_TH, line_width=3, color="red", alpha=0.5, legend_label="Target HI-index")
p1.line( x, y_PR, line_width=3, color="black", alpha=0.5, legend_label="PageRank")
p1.legend.location = "bottom_right"
tab1 = Panel(child=p1, title="Likes")

y_TH = 1.6076362207515547 * x + (-0.3104812947414938)
y_PR = 2.7391436344616076 * x + (-0.8381966907009905)
p2 = figure(plot_width=400, plot_height=300,
    x_range=(0, 1), y_range=(0, 1), 
    y_axis_label='Seeding Ratio (Female)',
    x_axis_label='Target Ratio (Female)')
p2.line( x, y_TH, line_width=3, color="red", alpha=0.5, legend_label="Target HI-index")
p2.line( x, y_PR, line_width=3, color="black", alpha=0.5, legend_label="PageRank")
p2.legend.location = "bottom_right"
tab2 = Panel(child=p2, title="Comments")
tabs = Tabs(tabs=[ tab1, tab2 ])
tabs.on_change('active', lambda attr, old, new: update())

# radios = RadioButtonGroup(labels=["Likes", "Comments"])
# radios.on_change('active', lambda attr, old, new: update())
button = Button(label="Download Selected Seeds (Target HI-index)", button_type="success", height=40)
# src = ColumnDataSource(data=dict())

button.js_on_click(CustomJS(args=dict(source=source), code=open(join(dirname(__file__), "download.js")).read()))
button_PR = Button(label="Download Selected Seeds (PageRank)", button_type="success", height=40)
button_PR.js_on_click(CustomJS(args=dict(source=source_PR), code=open(join(dirname(__file__), "download.js")).read()))

template="""
<div style="background:<%= 
    (function colorfromint(){
        if(value == "M"){
            return("navy")}
        else{return("red")}
        }()) %>; 
    color: white"> 
<%= value %></div>
"""

formater =  HTMLTemplateFormatter(template=template)
columns = [
    TableColumn(field="gender", title="Gender", formatter=formater),
    TableColumn(field="userID", title="User Identity"),
    TableColumn(field="rank_value", title="Target HI-index"),
    TableColumn(field="freq", title="Intensity"),
    TableColumn(field="indegree", title="Degree")]
data_table = DataTable(source=source, columns=columns, width=550, height=700)
columns_PR = [
    TableColumn(field="gender", title="Gender", formatter=formater),
    TableColumn(field="userID", title="User Identity"),
    TableColumn(field="rank_value", title="PageRank"),
    TableColumn(field="freq", title="Intensity"),
    TableColumn(field="indegree", title="Degree")]
data_table_PR = DataTable(source=source_PR, columns=columns_PR, width=550, height=700)
dumdiv = Div(text='', height=12)
dumdiv_w = Div(text='', width=14)

div = Div(text=f"Target HI-index: Selected [Female, Male] Users: [{gender_dist.data['value'][0]}, {gender_dist.data['value'][1]}]")
gender_dist.js_on_change('data', CustomJS(args=dict(d=div),
    code="d.text = `Target HI-index: Selected [Female, Male] Users: [${cb_obj.data['value'][0]}, ${cb_obj.data['value'][1]}]`"))

div_PR = Div(text=f"PageRank: Selected [Female, Male] Users: [{gender_dist.data['value'][0]}, {gender_dist.data['value'][1]}]")
gender_dist_PR.js_on_change('data', CustomJS(args=dict(d=div_PR),
    code="d.text = `PageRank: Selected [Female, Male] Users: [${cb_obj.data['value'][0]}, ${cb_obj.data['value'][1]}]`"))

b = Button(label="User number update")
b.js_on_click(CustomJS(args=dict(ds=gender_dist, ds_pr=gender_dist_PR), 
    code="ds.properties.data.change.emit();ds_pr.properties.data.change.emit();"))

controls = column(tabs, K_input, TR_slider_TH, TR_slider_PR, div, div_PR, b,dumdiv, button, button_PR)
curdoc().add_root(row(controls, dumdiv_w, data_table, data_table_PR))
curdoc().title = "Disparity Seeding"
update()


