from pandas.core.common import flatten
gender_dict = {}
with open("./gender_mapping.csv", 'r') as read_file:
    headers = read_file.readline()
    for line in read_file:
        token = line.strip().split(',')
        gender_dict[token[0]] = token[1]

interact_dict = [{}, {}]
# for byType in range(2):
for byType in [0]:
    with open('./frequency/Frequency_sort_{}.csv'.format(byType)) as freq:
        for line in freq:
            token1 = line.strip().split(',')
            try: interact_dict[byType][token1[0]] = [int(token1[1][:-2]), 0]
            except: interact_dict[byType][token1[0]] = [0, 0]

    with open('./indegree/Indegree_sort_{}.csv'.format(byType)) as indegree:            
        for line in indegree:
            token2 = line.strip().split(',')
            try: interact_dict[byType][token2[0]][1] = int(token2[1][:-2])
            except: interact_dict[byType][token2[0]][1] = 0
    
    # for i in range(11):
    for i in [10]:
        with open('./hi-index/concat/HIindex_sort_{}_{}.csv'.format(byType, i/10), 'w') as hi_idx_write:
            with open('./hi-index/HIindex_sort_{}_{}.csv'.format(byType, i/10), 'r') as hi_idx:
                for line in hi_idx:
                    token = line.strip().split(',')
                    hi_idx_write.write("{},{},{},{},{}\n".format(*list(flatten([gender_dict[token[0]], token, interact_dict[byType][token[0]]]))))
    
    # with open('./pagerank/concat/Pagerank_sort_{}.csv'.format(byType), 'w') as pr_write:
    #         with open('./pagerank/Pagerank_sort_{}.csv'.format(byType), 'r') as pr:
    #             for line in pr:
    #                 token = line.strip().split(',')
    #                 pr_write.write("{},{},{},{},{}\n".format(*list(flatten([gender_dict[token[0]], token, interact_dict[byType][token[0]]]))))
                    
